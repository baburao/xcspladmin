$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('getkycfilesdata', function(data){
        $('#banktransferwithdates').DataTable({
        data: data.result,
        dom: 'Bfrtip',

        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Mobile No" },
            { title: "Email" },
            { title: "Pan No" },
            { title: "Aadhar No" },
            {"mRender": function ( data, type, row ) {
                var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                return '<img class="imgres" src="'+data1+row[9]+'">'

            ;}

            }, {"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[10]+'">'

              ;}

           },{"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[11]+'">'

            ;}

           },{"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[12]+'">'

           ;}

    },{ title: "Status" },
    {"mRender": function ( data, type, row ) {
        return '<div  data-id="' + row[4] + '" onclick="Getinfo('+row[14]+');">Edit</div>'
    ;}
}
        ]
    } );
    $('.loading').removeClass('spinner');
});
});

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var postdata = '{"fromdate":"'+fromdate+'","todate":"'+todate+'"}';
    app.post('kycfiles',postdata,function(data){
        $('#banktransferwithdates').DataTable({
            data: data.result,
            dom: 'Bfrtip',
            "bDestroy": true,
            "scrollY":false,
            "scrollX":true,
            "pageLength": 12,
            buttons: [
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-download fa-2x"></i>',
                    titleAttr: 'PDF'
                }
            ],
                "bDestroy": true,
            columns: [


                { title: "Date" },
                { title: "Time" },
                { title: "Pia Type" },
                { title: "Pia Id" },
                { title: "Pia Name" },
                { title: "Mobile No" },
                { title: "Email" },
                { title: "Pan No" },
                { title: "Aadhar No" },
                {"mRender": function ( data, type, row ) {
                    var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                    return '<img class="imgres" src="'+data1+row[9]+'">'

                ;}

                }, {"mRender": function ( data, type, row ) {
                var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                return '<img class="imgres" src="'+data1+row[10]+'">'

                  ;}

               },{"mRender": function ( data, type, row ) {
                var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                return '<img class="imgres" src="'+data1+row[11]+'">'

                ;}

               },{"mRender": function ( data, type, row ) {
                var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                return '<img class="imgres" src="'+data1+row[12]+'">'

               ;}

        },{ title: "Status" },
        {"mRender": function ( data, type, row ) {
            return '<div  data-id="' + row[4] + '" onclick="Getinfo('+row[14]+');">Edit</div>'
        ;}
    }
            ]
        } );
        $('.loading').removeClass('spinner');
    })
});

function Getinfo(id) {

    var postdata2 = '{"kycid":"'+id+'"}';
app.post('updatekycstatus',postdata2,function(data){
    UpdateStatus();
});
}

function UpdateStatus(){
    $('.loading').addClass('spinner');
    app.get('getkycfilesdata', function(data){
        $('#banktransferwithdates').DataTable({
        data: data.result,
        dom: 'Bfrtip',
        "bDestroy": true,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Mobile No" },
            { title: "Email" },
            { title: "Pan No" },
            { title: "Aadhar No" },
            {"mRender": function ( data, type, row ) {
                var data1 = 'http://127.0.0.1:8000/content/affiliate/'
                return '<img class="imgres" src="'+data1+row[9]+'">'

            ;}

            }, {"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[10]+'">'

              ;}

           },{"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[11]+'">'

            ;}

           },{"mRender": function ( data, type, row ) {
            var data1 = 'http://127.0.0.1:8000/content/affiliate/'
            return '<img class="imgres" src="'+data1+row[12]+'">'

           ;}

    },{ title: "Status" },
    {"mRender": function ( data, type, row ) {
        return '<div  data-id="' + row[4] + '" onclick="Getinfo('+row[14]+');">Edit</div>'
    ;}
}
        ]
    } );
    $('.loading').removeClass('spinner');
});
}



