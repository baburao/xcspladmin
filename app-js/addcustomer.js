
$(document).ready(function() {
 
  $(".submitbtn").addClass("hide");
  $(".shippngaddress").addClass("hide");
});


$(".userregistrationform").click(function(){

  $("#msg").empty();
  $('.loading').addClass('spinner');
  var Investor1 = $('.Investor').is(':checked');
  var Affiliate1 = $('.Affiliate').is(':checked');
  var AffiliateEdp1 = $('.AffiliateEdp').is(':checked');
 if(Investor1==true){
  var investor = 'Investor'
 }
 if(Affiliate1==true){
  var investor = 'Affiliate'
  }
  if(AffiliateEdp1==true){
     var investor = 'AffiliateEdp'
  }
 
 
 

 title = $("#titlemr").val();                   country = $("#country").val();
 firstname = $("#firstname").val();             state = $("#state").val();
 lastname = $("#lastname").val();               district = $("#district").val();
 fathername = $("#fathername").val();           city = $("#city").val();
 mothername = $("#mothername").val();           addressline2 = $("#address2").val();
 dob = $("#dob").val();                         addressline1 = $("#address1").val();
 pincode = $("#pincode").val();                 panno = $("#panno").val();
 saddressline1 = $("#saddressline1").val();     nomineename = $("#nomineename").val();
 saddressline2 = $("#saddressline2").val();     nomineer = $("#nomineer").val();
 scity = $("#scity").val();                     bankacno = $("#bankacno").val();
 sdistrict = $("#sdistrict").val();             bankholdername = $("#bankholdername").val();
 ssate = $("#ssate").val();                     bankname = $("#bankname").val();
 scountry = $("#scountry").val();               bankbranch = $("#bankbranch").val();
 spincode = $("#spincode").val();               ifsccode = $("#ifsccode").val();
 aadharno = $("#aadharno").val();               actype = $("#actype").val();
 mobileno = $("#mobileno").val();               emailid = $("#emailid").val();
 password = $("#password").val();               retranspassword = $("#retranspassword").val();
 repassword = $("#repassword").val();           transpassword = $("#transpassword").val();
 




 if(title==""){
     $('.alertMsg').removeClass('cli');
     $('.alertMsg').addClass('dangerMsg');
     $('#msg').append('Please enter title...!!!')
     //api.removeVendor()
     return
 }
 if(firstname==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the first name...!!!')
  //api.removeVendor()
  return
}
if(lastname==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the last name...!!!')
  //api.removeVendor()
  return
}
if(fathername ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter father name...!!!')
  //api.removeVendor()
  return
}
if(mothername ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter mother name...!!!')
  //api.removeVendor()
  return
}
if(dob ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter date of birth...!!!')
  //api.removeVendor()
  return
}

if(addressline1 ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the address line 1...!!!')
 // api.removeVendor()
  return
}
if(addressline2 ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the address line 2...!!!')
  //api.removeVendor()
  return
}
if(city ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the city...!!!')
 // api.removeVendor()
  return
}

if(district ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the district...!!!')
  //api.removeVendor()
  return
}
if(state ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the state...!!!')
  //api.removeVendor()
  return
}
if(country ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the country...!!!')
  //api.removeVendor()
  return
}
if(pincode ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter pincode...!!!')
  //api.removeVendor()
  return
}

if(pincode !=""){

  if (/^\d{6}$/.test(pincode)) {
     //alert("It's 6 digits.");
 } else {
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the valid pincode...!!!')
  //api.removeVendor()
  return
 }
}

if(aadharno ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter aadhar number...!!!')
  //api.removeVendor()
  return
}

if(aadharno !=""){
  var aadhar = document.getElementById("aadharno").value;
  var adharcardTwelveDigit = /^\d{12}$/;
  var adharSixteenDigit = /^\d{16}$/;
  if (aadhar != '') {
      if (aadhar.match(adharcardTwelveDigit)) {
        
      }
      else if (aadhar.match(adharSixteenDigit)) {
        
      }
      else {
           $('.alertMsg').removeClass('cli');
           $('.alertMsg').addClass('dangerMsg');
           $('#msg').append('Please enter valid aadhar number...!!!')
          return;
      }
  }
 }

if(panno ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter pan number...!!!')
  //api.removeVendor()
  return
}
if(panno !=""){
  var Obj = document.getElementById("panno");
  if (Obj.value != "") {
      ObjVal = Obj.value;
      var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
      if (ObjVal.search(panPat) == -1) {
           $('.alertMsg').removeClass('cli');
           $('.alertMsg').addClass('dangerMsg');
           $('#msg').append('Please enter valid pan number...!!!')
          Obj.focus();
          return false;
      }

  }
  }
if(nomineer ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter nominee relation...!!!')
  //api.removeVendor()
  return
}

if(nomineename ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter nominee name...!!!')
 // api.removeVendor()
  return
}
if(bankacno ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the bank account number...!!!')
  //api.removeVendor()
  return
}

if(bankacno !=""){
  var re = /^([0-9]{11})|([0-9]{2}-[0-9]{3}-[0-9]{6})$/;
  if (re.test(bankacno)==true) {
    
  }
  else {
     $('.alertMsg').removeClass('cli');
     $('.alertMsg').addClass('dangerMsg');
     $('#msg').append('Please enter title...!!!')
     //api.removeVendor()
     return
   
  }
}
if(bankholdername ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the bank account holder name...!!!')
  //api.removeVendor()
  return
}
if(bankname ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the bank name...!!!')
  //api.removeVendor()
  return
}
if(bankbranch ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the branch name...!!!')
 // api.removeVendor()
  return
}
if(ifsccode ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter ifsc code...!!!')
  //api.removeVendor()
  return
}
if(actype ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the account type...!!!')
  //api.removeVendor()
  return
}
if(mobileno ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter mobile number...!!!')
  //api.removeVendor()
  return
}

if(mobileno !=""){
  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if((mobileno.match(phoneno))){
     
  }else{
     $('.alertMsg').removeClass('cli');
     $('.alertMsg').addClass('dangerMsg');
     $('#msg').append('Please enter valid mobile number...!!!')
     //api.removeVendor()
     return
  }
}
if(emailid ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the email id...!!!')
  //api.removeVendor()
  return
}

if(emailid!=""){
  var regEx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      var validEmail = regEx.test(emailid);
      if (!validEmail) {
          $('.alertMsg').removeClass('cli');
          $('.alertMsg').addClass('dangerMsg');
          $('#msg').append('Please enter valid email address...!!!')
          //api.removeVendor()
          return
      }
  }
if(password ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter password...!!!')
  //api.removeVendor()
  return
}
if(repassword ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter repassword...!!!')
  //api.removeVendor()
  return
}
if(password!=repassword){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter password and conform must be same...!!!')
  //api.removeVendor()
  return
}
if(transpassword ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the transaction password...!!!')
 // api.removeVendor()
  return
}
if(retranspassword ==""){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the re enter transaction password...!!!')
  //api.removeVendor()
  return
}

if(transpassword!=retranspassword){
  $('.alertMsg').removeClass('cli');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append('Please enter the transaction password and conform transaction passowrd must be same...!!!')
//  api.removeVendor()
  return
}


 


 var postdata = '{"piatype":"'+investor+'","titlemr":"'+title+'","firstname":"'+firstname+'","lastname":"'+lastname+'","fathername":"'+fathername+'","mothername":"'+mothername+'","dob":"'+dob+'","addressline1":"'+addressline1+'","addressline2":"'+addressline2+'","city":"'+city+'","district":"'+district+'","state":"'+state+'","country":"'+country+'","pincode":"'+pincode+'","saddressline1":"'+saddressline1+'","saddressline2":"'+saddressline2+'","scity":"'+scity+'","sdistrict":"'+sdistrict+'","ssate":"'+ssate+'","scountry":"'+scountry+'","spincode":"'+spincode+'","aadharno":"'+aadharno+'","panno":"'+panno+'","nomineename":"'+nomineename+'","nomineer":"'+nomineer+'","bankacno":"'+bankacno+'","bankholdername":"'+bankholdername+'","bankname":"'+bankname+'","bankbranch":"'+bankbranch+'","ifsccode":"'+ifsccode+'","actype":"'+actype+'","mobileno":"'+mobileno+'","emailid":"'+emailid+'","password":"'+password+'","repassword":"'+repassword+'","transpassword":"'+transpassword+'","retranspassword":"'+retranspassword+'"}'
 
 
  
  api.post('userregistration',postdata,function(data){
     $('.loading').removeClass('spinner');
     location.href="login-page.html";
  });

  
})

$(".showshippingadd").click(function(){

   var data = $('.showshippingadd').is(':checked');

 if(data==true){
  $(".shippngaddress").removeClass("hide");
     addressline1 = $("#address1").val();
     addressline2 = $("#address2").val();
     city = $("#city").val();
     district = $("#district").val();
     state = $("#state").val();
     country = $("#country").val();
     pincode = $("#pincode").val();
     $("#saddress1").val(addressline1);
     $("#saddress2").val(addressline2);
     $("#scity").val(city);
     $("#sdistrict").val(district);
     $("#sstate").val(state);
     $("#scountry").val(country);
     $("#spincode").val(pincode);


  

 }else{
  $(".shippngaddress").addClass("hide");
  }
  

});


$(".backbtn").click(function(){
  location.href="login-page.html";
})


$('#verifyuser').keyup(function (event) {
  $('.loading').addClass('spinner');
 
 addressline1 = $("#verifyuser").val();
//   alert(addressline1)
 var postdata = '{"piaid":"'+addressline1+'"}'
  api.post('getinterducername',postdata,function(data){
     $('.loading').removeClass('spinner');
      if(data.result.length!=0){
     var interducername = "Introducer Name : "+data.result[0]['nomineefullname'];
     $('.intername').append(interducername);
     $(".submitbtn").removeClass("hide");
      }
      else{
         var interducername = '';
         $('.intername').append('');
         $(".submitbtn").addClass("hide");
      }
  });

});


$(".submitbtn").click(function(){

  location.href='registrationpage.html';
});