$(document).ready(function() {
  $('.loading').addClass('spinner');
  app.get('getaffiliatereportadmin', function(data){
      $('#banktransferwithdates').DataTable({
      data: data.result,
      dom: 'Bfrtip',
      buttons: [
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-download fa-2x"></i>',
            titleAttr: 'PDF'
        }
    ],
      columns: [
          
          
          { title: "Date" },
          { title: "Pia type" },
          { title: "Pia Id" },
          { title: "Title" },
          { title: "First Name" },
          { title: "Last Name" },
          { title: "Father Name" },
          { title: "Mother Name" },
          { title: "Date Of Birth" },
          { title: "Address Line 1" },
          { title: "Address Line 2" },
          { title: "City" },
          { title: "District" },
          { title: "State" },
          { title: "Country" },
          { title: "Pin Code" },
          { title: "Shipping Address Line 1" },
          { title: "Shipping Address Line 2" },
          { title: "City" },
          { title: "District" },

          { title: "State" },
          { title: "Country" },
          { title: "Aadhar No" },
          { title: "Pan No" },
          { title: "Nominee Full Name" },
          { title: "Nominee Relationship" },
          { title: "Bank Account Number" },
          { title: "Holder Name" },
          { title: "Bank Name" },
          { title: "Branch" },
          { title: "IFSC Code" },
          { title: "Account type" },
          { title: "Mobile No" },
          { title: "Email" },
          { title: "Rank" },
          { title: "Amount" },
          { title: "Unins" },

         
      ]
  } );
  $('.loading').removeClass('spinner');
});
});

$(".selfsearch").click(function(){
  $('.loading').addClass('spinner');
  var fromdate = $('#fromdate').val();
  var todate = $('#todate').val();
  var postdata = '{"fromdate":"'+fromdate+'","todate":"'+todate+'"}';
  app.post('getaffiliatereportadminwithdates',postdata,function(data){
      var d = data.result;
      $('#banktransferwithdates').DataTable({
          data: data.result,
          dom: 'Bfrtip',
          "bDestroy": true,
          buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
          columns: [
              
              
              { title: "Date" },
              { title: "Pia type" },
              { title: "Pia Id" },
              { title: "Title" },
              { title: "First Name" },
              { title: "Last Name" },
              { title: "Father Name" },
              { title: "Mother Name" },
              { title: "Date Of Birth" },
              { title: "Address Line 1" },
              { title: "Address Line 2" },
              { title: "City" },
              { title: "District" },
              { title: "State" },
              { title: "Country" },
              { title: "Pin Code" },
              { title: "Shipping Address Line 1" },
              { title: "Shipping Address Line 2" },
              { title: "City" },
              { title: "District" },
  
              { title: "State" },
              { title: "Country" },
              { title: "Aadhar No" },
              { title: "Pan No" },
              { title: "Nominee Full Name" },
              { title: "Nominee Relationship" },
              { title: "Bank Account Number" },
              { title: "Holder Name" },
              { title: "Bank Name" },
              { title: "Branch" },
              { title: "IFSC Code" },
              { title: "Account type" },
              { title: "Mobile No" },
              { title: "Email" },
              { title: "Rank" },
              { title: "Amount" },
              { title: "Unins" },
          ]
      } );
      $('.loading').removeClass('spinner');
  });
  })


