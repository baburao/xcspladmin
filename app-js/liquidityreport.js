$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('getliquidityreportadmin', function(data){
        var dd = data.result;
        $('#banktransferwithdates').DataTable({
        data: data.result,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Intro Id" },
            { title: "Intro Name" },
            { title: "Intro Payout" },
            { title: "Rank" },
            { title: "Mobile" },
            { title: "Email" },
            { title: "Aadhar No" },
            { title: "Pan No" },
            { title: "Address Line 1" },
            { title: "Address Line 2" },
            { title: "City" },
            { title: "District" },
            { title: "State" },
            { title: "Country" },

            { title: "Pincode" },
            { title: "Face Value" },
            { title: "Charges" },
            { title: "Intro Commission" },
            { title: "Intro TDS" },
            { title: "Intro Net Commission" },
            { title: "Partner Commission" },
            { title: "Partner TDS" },
            { title: "Partner Net" },
            { title: "Total Inflow" },
            { title: "Total Outflow" },
            { title: "Remaining Amount" }

        ]
    } );

    $('.loading').removeClass('spinner');
});
});

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var postdata = '{"fromdate":"'+fromdate+'","todate":"'+todate+'"}';
    app.post('getliquidityreportadminwithdates',postdata,function(data){
        $('#banktransferwithdates').DataTable({
            data: data.result,
            dom: 'Bfrtip',
            "bDestroy": true,
            "scrollY":false,
            "scrollX":true,
            "pageLength": 12,
            buttons: [
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-download fa-2x"></i>',
                    titleAttr: 'PDF'
                }
            ],
                "bDestroy": true,
            columns: [

                { title: "Date" },
                { title: "Time" },
                { title: "Pia Type" },
                { title: "Pia Id" },
                { title: "Pia Name" },
                { title: "Intro Id" },
                { title: "Intro Name" },
                { title: "Intro Payout" },
                { title: "Rank" },
                { title: "Mobile" },
                { title: "Email" },
                { title: "Aadhar No" },
                { title: "Pan No" },
                { title: "Address Line 1" },
                { title: "Address Line 2" },
                { title: "City" },
                { title: "District" },
                { title: "State" },
                { title: "Country" },

                { title: "Pincode" },
                { title: "Face Value" },
                { title: "Charges" },
                { title: "Intro Commission" },
                { title: "Intro TDS" },
                { title: "Intro Net Commission" },
                { title: "Partner Commission" },
                { title: "Partner TDS" },
                { title: "Partner Net" },
                { title: "Total Inflow" },
                { title: "Total Outflow" },
                { title: "Remaining Amount" }




            ]
        } );
        $('.loading').removeClass('spinner');
    })
});



