$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('selfbankdepositedata', function(data){
        depositetable(data.result)

});
});

function depositetable(data){
    $('#banktransferwithdates').DataTable({
        data: data,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Mobile" },
            { title: "Email" },
            { title: "Amount in INR" },
            { title: "Reference Number" },
            { title: "Type Of deposite" },
            { title: "Remarks" },
            { title: "Status" },
            {"mRender": function ( data, type, row ) {
            return '<div  data-id="' + row[12] + '" onclick="Getinfo('+row[12]+');">Edit</div>'
        ;}
            }

        ]
    } );
    $('.loading').removeClass('spinner');
}

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var fromSplit = fromdate.split("/");
    var toSplit = todate.split("/");
    var formdate1 = fromSplit[2]+"-"+fromSplit[0]+"-"+fromSplit[1];
    var todate1 = toSplit[2]+"-"+toSplit[0]+"-"+toSplit[1]
    
    var postdata = '{"fromdate":"'+formdate1+'","todate":"'+todate1+'"}';
    app.post('selfbankdepositereport',postdata,function(data){
        depositetable(data.result)
    });
    })

    function Getinfo(id) {
        var postdata2 = '{"status":"'+id+'"}'
    app.post('updatebankdepositestatus',postdata2,function(data){
        updateSelfBankDeposite();

    });
    }

    function updateSelfBankDeposite(){
        $('.loading').addClass('spinner');
        app.get('selfbankdepositedata', function(data){
            depositetable(data.result)
    });
    }
