$(document).ready(function() {
  $('.loading').addClass('spinner');
  app.get('gettdscommissionadmindata', function(data){
    TDS(data.result);
});
});

function TDS(data){
    $('#banktransferwithdates').DataTable({
        data: data,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-download fa-2x"></i>',
              titleAttr: 'PDF'
          }
      ],
        columns: [
  
  
            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Mobile" },
            { title: "Email" },
            { title: "Aadhar No" },
            { title: "Pan No" },
            { title: "Address Line 1" },
            { title: "Address Line 2" },
            { title: "City" },
            { title: "District" },
            { title: "State" },
            { title: "Country" },
  
            { title: "Pin Code" },
            { title: "Buyer Id" },
            { title: "Buyer Name" },
            { title: "Buyer No Of Units" },
            { title: "Payout Rank" },
            { title: "Total Commission" },
            { title: "TDS Amount" },
            { title: "Net Amount" },
  
            {"mRender": function ( data, type, row ) {
            return '<div  data-id="' + row[4] + '" onclick="Getinfo('+row[15]+');" onclick="editProd('+row[4]+');">Edit</div>'
        ;}
            }
        ]
    } );
    $('.loading').removeClass('spinner');
}

$(".selfsearch").click(function(){
  $('.loading').addClass('spinner');
  var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var fromSplit = fromdate.split("/");
    var toSplit = todate.split("/");
    var formdate1 = fromSplit[2]+"-"+fromSplit[0]+"-"+fromSplit[1];
    var todate1 = toSplit[2]+"-"+toSplit[0]+"-"+toSplit[1]
    
    var postdata = '{"fromdate":"'+formdate1+'","todate":"'+todate1+'"}';
  app.post('tdscommissionreport',postdata,function(data){
    TDS(data.result);
  });
  })

  function Getinfo(id) {
      var postdata2 = '{"status":"'+id+'"}'
  app.post('updatebanktransferstatus',postdata2,function(data){
      $('#date').val(data.result[0].date);
      $('#walkid').val(data.result[0].id);
      $('#time').val(data.result[0].time);
      $('#clientname').val(data.result[0].clientname);
      $('#mobileno').val(data.result[0].mobile);
      $('#emailid').val(data.result[0].email);
      $('.walkinedit').removeClass('cli');
      $('.salestable').addClass('cli');
      $('.walkintable').addClass('cli');

  });
  }


