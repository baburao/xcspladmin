$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('selfbanktransferreport', function(data){
        $('#e-Statement').DataTable({
        data: data.result,
        dom: 'Bfrtip',
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [
            
            { title: "Sl No." },
            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Bank Account Number" },
            { title: "Bank Account Holder Name" },
            { title: "Bank" },
            { title: "Branch" },
            { title: "IFSC Code" },
            { title: "Account Type" },
            { title: "Mobile" },
            { title: "Email Id" },
            { title: "Amount in INR" }
        ]
    } );
});   
$('.loading').removeClass('spinner');
});
