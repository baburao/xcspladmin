$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('getreportdataadmin', function(data){
        reports(data.result);

});
});

function reports(data){
    $('#banktransferwithdates').DataTable({
        data: data,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Father Name" },
            { title: "Mobile No" },
            { title: "Email" },
            { title: "Aadhar No" },
            { title: "Panno" },
            { title: "Address Line 1" },
            { title: "Address Line 2" },
            { title: "City" },
            { title: "District" },
            { title: "State" },
            { title: "Country" },
            { title: "Pincode" },
            { title: "Shipping Address Line 1" },
            { title: "Shipping Address Line 2" },
            { title: "city" },
            { title: "District" },
            { title: "State" },
            { title: "Country" },
            { title: "Pincode" },
            { title: "No Of Units" },
            { title: "Amount" }

        ]
    } );
    $('.loading').removeClass('spinner');
}

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var fromSplit = fromdate.split("/");
    var toSplit = todate.split("/");
    var formdate1 = fromSplit[2]+"-"+fromSplit[0]+"-"+fromSplit[1];
    var todate1 = toSplit[2]+"-"+toSplit[0]+"-"+toSplit[1]
    
    var postdata = '{"fromdate":"'+formdate1+'","todate":"'+todate1+'"}';
    app.post('getreportdataadminwithdates',postdata,function(data){
        reports(data.result);
    })
});



