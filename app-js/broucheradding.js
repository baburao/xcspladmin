$("button.broucher").click(function(){
    $('.loading').addClass('spinner');
    title = $("#title").val();
    description = $("#description").val();
    qty = $("#qty").val();
    total = $("#total").val();
    if(title==""){
        $('.alertMsg').removeClass('cli');
        $('.alertMsg').addClass('dangerMsg');
        $('#msg').append('Please enter title...!!!')
        app.removeVendor()
        $('.loading').removeClass('spinner');
        return;
    }
    if(description==""){
        $('.alertMsg').removeClass('cli');
        $('.alertMsg').addClass('dangerMsg');
        $('#msg').append('Please enter description...!!!')
        app.removeVendor()
        $('.loading').removeClass('spinner');
        return;
    }
    if(qty==""){
        $('.alertMsg').removeClass('cli');
        $('.alertMsg').addClass('dangerMsg');
        $('#msg').append('Please enter quantity...!!!')
        app.removeVendor()
        $('.loading').removeClass('spinner');
        return;
    }
    if(total==""){
        $('.alertMsg').removeClass('cli');
        $('.alertMsg').addClass('dangerMsg');
        $('#msg').append('Please enter total...!!!')
        app.removeVendor()
        $('.loading').removeClass('spinner');
        return;
    }
    var postdata = '{"title":"'+title+'","description":"'+description+'","qty":"'+qty+'","total":"'+total+'"}';
    app.post('brouchersadding',postdata,function(data){

$('.loading').removeClass('spinner');
    });

	
})