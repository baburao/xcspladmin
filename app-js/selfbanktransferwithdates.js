$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('adminselfbanktransfer', function(data){
    getselfbanktransferdata(data.result)
    });
});
function getselfbanktransferdata(data){
   
        var d = $('#banktransferwithdates').DataTable({
        data: data,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
       "scrollX":true,
       "pageLength": 12,
       buttons: [
        {
            extend:    'pdfHtml5',
            text:      '<i class="fa fa-download fa-2x"></i>',
            titleAttr: 'PDF'
        }
    ],
        columns: [
            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Bank Account Number" },
            { title: "Bank Account Holder Name" },
            { title: "Bank" },
            { title: "Branch" },
            { title: "IFSC Code" },
            { title: "Account Type" },
            { title: "Mobile" },
            { title: "Email Id" },
            { title: "Amount in INR" },
            { title: "Status" },
            
            {"mRender": function ( data, type, row ) {
            return '<div  data-id="' + row[4] + '" onclick="Getinfo('+row[15]+');" onclick="editProd('+row[4]+');">Edit</div>'
        ;}
            }
        ],

    } );


    $('.loading').removeClass('spinner');

}

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var fromSplit = fromdate.split("/");
    var toSplit = todate.split("/");
    var formdate1 = fromSplit[2]+"-"+fromSplit[0]+"-"+fromSplit[1];
    var todate1 = toSplit[2]+"-"+toSplit[0]+"-"+toSplit[1]
    
    var postdata = '{"fromdate":"'+formdate1+'","todate":"'+todate1+'"}';
    app.post('selfbanktransferreport',postdata,function(data){
        var d = data.result;
        getselfbanktransferdata(data.result)

    });
    })

    function Getinfo(id) {
        var postdata2 = '{"status":"'+id+'"}'
    app.post('updatebanktransferstatus',postdata2,function(data){
        Updateselfbanktransfer();

    });
    }

    function Updateselfbanktransfer(){
        $('.loading').addClass('spinner');
        app.get('adminselfbanktransfer', function(data){
            getselfbanktransferdata(data.result)
    });
    }


