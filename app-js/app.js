app = new Object();
app.ver = adminSettings.Version;
app.Root = adminSettings.Url;
app.Url = app.Root + "/api/" + app.ver + "/";

app.post = function(actionUrl,parms,callBack){
    $.post(app.Url+actionUrl,parms,function(data){
        callBack(data);
    }).fail(function(error){
        Errorlog(app.Url+actionUrl,error,parms);
    });
};

app.get = function(actionUrl, callBack){
    $.get(app.Url+actionUrl, function(data){
        callBack(data);
    }).fail(function(error){
        Errorlog(app.Url+actionUrl, error);
    });
};

function Errorlog(url,error, postdata){
    console.log(url);
    console.log(error);
    console.log(postdata);
    console.log(error.responseText);
}

app.removeVendor=function()
{
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
       return;
    });
};