$(document).ready(function() {
    $('.loading').addClass('spinner');
    app.get('requestformdata', function(data){
        var dd = data.result;
        $('#banktransferwithdates').DataTable({
        data: data.result,
        dom: 'Bfrtip',
        "bDestroy": true,
        "scrollY":false,
        "scrollX":true,
        "pageLength": 12,
        buttons: [
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-download fa-2x"></i>',
                titleAttr: 'PDF'
            }
        ],
        columns: [


            { title: "Date" },
            { title: "Time" },
            { title: "Pia Type" },
            { title: "Pia Id" },
            { title: "Pia Name" },
            { title: "Subject" },
            { title: "Matter" },
            { title: "Remarks" },
            { title: "Status" }
        ]
    } );

    $('.loading').removeClass('spinner');
});
});

$(".selfsearch").click(function(){
    $('.loading').addClass('spinner');
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var postdata = '{"fromdate":"'+fromdate+'","todate":"'+todate+'"}';
    app.post('customerrequestform',postdata,function(data){
        $('#banktransferwithdates').DataTable({
            data: data.result,
            dom: 'Bfrtip',
            "bDestroy": true,
            "scrollY":false,
            "scrollX":true,
            "pageLength": 12,
            buttons: [
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-download fa-2x"></i>',
                    titleAttr: 'PDF'
                }
            ],
                "bDestroy": true,
            columns: [

                { title: "Date" },
                { title: "Time" },
                { title: "Pia Type" },
                { title: "Pia Id" },
                { title: "Pia Name" },
                { title: "Subject" },
                { title: "Matter" },
                { title: "Remarks" },
                { title: "Status" }




            ]
        } );
        $('.loading').removeClass('spinner');
    })
});



